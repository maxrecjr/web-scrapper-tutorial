import scrapy

class QuotesSpider(scrapy.Spider):
    name = "quotes"

    start_urls = ['http://quotes.toscrape.com/']

    def parse(self, response):
        self.logger.info('hello this is my first spider')
        quotes = response.css('div.quote')
        for q in quotes:
            yield {
                'text':q.css('.text::text').get(),
                'author':q.css('.author::text').get(),
                'tags':q.css('.tag::text').get()
            }
        next_page = response.css('li.netx a::attr(href)').get()
        if next_page is not None:
            next_page = response.urljoin(next_page)
            yield scrapy.Request(next_page, callback=self.parse)